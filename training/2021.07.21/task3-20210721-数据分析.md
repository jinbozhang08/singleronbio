### 数据分析

#### 2021.7.7
 * 背景：日常工作内容会涉及对数据进行画图展示和算相关的统计差异值，有时候还涉及根据项目需要采取新的R包进行分析画图。本题一共25分。
 * 题目说明：本题的考核安装新的R包UpSet和对多个集合进行韦恩图分析，生成含有g1~g8的8个样品的UpSet图`sample.upseq.png`和生成根据下面分组信息的4个组的`sample.upseq.png`。
   * *_diffexpressed.xls为差异gene list
   * 分组信息：g1,g2的并集为Good，g3和g4的并集Normal，g5和g6的并集为Bad，g7和g8的为Unknown

 * 要求：
   * 在自己的电脑安装R，然后安装UpSet包进行分析，相关代码和图片再上传到集群。
   * 对于`sample.upseq.png`, 要求分组信息在图中的顺序为(从上到下): Good, Normal, Bad, Unknown
   * 生成ReadMe.txt, 里面描述对这个图的简单说明。(5分)
   * 对图片进行颜色调整或者其他美观的调整，视情况进行额外加分5~10分。

 * Upset图例子
 
   ![Image text](https://gitlab.com/jinbozhang08/singleronbio/-/raw/main/training/source/cDCs_downs.png)
