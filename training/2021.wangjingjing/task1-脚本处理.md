### 脚本考核

#### 2021.7.7
 * 背景：日常工作内容会涉及到编写shell跑任务，如果样品数目非常多，这个时候需要写程序来写shell。
 * 题目说明：本题的考核利用linux命令生成file.list,然后程序读入file.list输出相关的shell:
   * 其中T关键字为Tumor，例如T10，T19，F为Normal，例如F2和F7，
   * shell中的`ugname`和`spname`均为样品名字，以`,`隔开，`sptype`根据样品为Tumor或者Normal，`expM`为输入的前面路径+文件list，`datatype`为多个文件就对应多少个`1`,注意最后一个元素最后不带`,`
 * 要求：
   * 根据提供的目录利用linux命令生成file.list(该目录会存在其他的无关文件), 记录命令到脚本cmd.sh (5分)
   * 编写程序，输入为file.list， 输出按照样品的分组的生成run_normal.sh和run_tumor.sh。提示：关键参数应该根据每一行生成 (25分)
   * 编程语言不限，但是不能用手写，代码规范简洁 

```shell
## file.list格式如下：
uniqname/GSM4088774_T10_gene_cell_exprs_table.csv
uniqname/GSM4088775_T19_gene_cell_exprs_table.csv
uniqname/GSM4088785_F2_gene_cell_exprs_table.csv
uniqname/GSM4088786_F7_gene_cell_exprs_table.csv

## run_tumor.sh
#!/bin/sh
source activate Seuratv3
matrix_path=/SGRNJ03/old/cancer/
./run_MultiSamples.V3.normal.R \
    --expM ${matrix_path}/uniqname/GSM4088774_T10_gene_cell_exprs_table.csv,${matrix_path}/uniqname/GSM4088775_T19_gene_cell_exprs_table.csv \
    --datatype 1,1 \
    --ugname T10,T19 \
    --spname T10,T19 \
    --sptype Tumor,Tumor \
    --prefix public_tumor \
    --mtfilter 10 \
    --dim 20 \
    --resolution 1 \
    --object 1,1,0,0,0,0 \
    --outdir /SGRNJ03/old/cancer/

## run_normal.sh
#!/bin/sh
source activate Seuratv3
matrix_path=/SGRNJ03/old/normal/

./run_MultiSamples.V3.tumor.R \
    --expM ${matrix_path}/uniqname/GSM4088785_F2_gene_cell_exprs_table.csv,${matrix_path}/uniqname/GSM4088786_F7_gene_cell_exprs_table.csv \
    --datatype 1,1 \
    --ugname F2,F7 \
    --spname F2,F7 \
    --sptype Normal,Normal \
    --prefix public_normal \
    --mtfilter 10 \
    --dim 20 \
    --resolution 1 \
    --object 1,1,0,0,0,0 \
    --outdir /SGRNJ03/old/normal/

```


 
