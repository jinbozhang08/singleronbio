#### 2021.7.7
 * 背景：日常工作内容会涉及数据处理(包括数据库)，因为网上下载的数据会存在格式不对应和里面有缺失值或者错误值，不符合下游的分析，因此需要根据需求调整输入格式和清洗数据:
 * 题目说明：本题考核对矩阵进行格式调整和输出符合我们流程的格式。
   * 任务一：对于heaeds.xls，利用sed命令去掉第一行的`"Symbol"`的名字，原地修改文件 (5分)
```
## 输入
"Symbol"        "AAACCCAAGAGTGTTA.1"    "AAACCCAAGCCTGGAA.1"    "AAACCCAAGCGGACAT.1"    "AAACCCAAGTCTGTAC.1"    "AAACCC
"MIR1302-2HG"   0       0       0       0       0       0       0       0       0       0       0       0       0
"FAM138A"       0       0       0       0       0       0       0       0       0       0       0       0       0
"OR4F5" 0       0       0       0       0       0       0       0       0       0       0       0       0       0
"AL627309.1"    0       0       0       0       0       0       0       0       0       0       0       0       0
## 输出
"AAACCCAAGAGTGTTA.1"    "AAACCCAAGCCTGGAA.1"    "AAACCCAAGCGGACAT.1"    "AAACCCAAGTCTGTAC.1"    "AAACCC
"MIR1302-2HG"   0       0       0       0       0       0       0       0       0       0       0       0       0
"FAM138A"       0       0       0       0       0       0       0       0       0       0       0       0       0
"OR4F5" 0       0       0       0       0       0       0       0       0       0       0       0       0       0
"AL627309.1"    0       0       0       0       0       0       0       0       0       0       0       0       0
```
   * 任务二：对duplicates.xls (35分)
     * 编程去掉矩阵的第一列`Gene_ID`, 
     * 将矩阵中`NA`变成`0`
     * 第二列会有重复的基因名字，采取去掉重复的行(保留第一次出现的)，输出结果以`,`隔开输出unique.csv

```
## 输入
Gene_ID Symbol  AAACCTGAGCTCAACT.1      AAACCTGAGGAGCGTT.1      AAACCTGCACCAGATT.1      AAAC
ENSG00000000003 TSPAN6  1       0       0       0       0       0       0       0       0
ENSG00000000419 DPM1    0       1       0       1       0       0       0       0       1
ENSG00000000457 SCYL3   0       0       0       0       0       1       0       1       0
ENSG00000000460 C1orf112        0       0       0       0       0       0       0       0
ENSG00000000938 FGR     0       0       NA      0       0       0       0       0       0
ENSG000000ii938 FGR     0       0       0       0       0       0       0       0       0
ENSG00000000971 CFH     0       0       0       0       0       0       0       0       0
ENSG00000001036 FUCA2   0       NA      0       1       0       0       0       0       0
ENSG00000001037 FUCA2   0       0       0       1       0       0       0       0       0
ENSG00000001084 GCLC    NA      0       0       NA      0       0       0       0       0
ENSG00000001167 NFYA    0       0       0       0       0       1       0       0       0

## 输出， 行的顺序不要求
Symbol,AAACCTGAGCTCAACT.1,AAACCTGAGGAGCGTT.1,AAACCTGCACCAGATT.1,AAAC
TSPAN6,1,0,0,0,0,0,0,0,0
DPM1,0,1,0,1,0,0,0,0,1
SCYL3,0,0,0,0,0,1,0,1,0
C1orf112,0,0,0,0,0,0,0,0
FGR,0,0,0,0,0,0,0,0,0
CFH,0,0,0,0,0,0,0,0,0
FUCA2,0,0,0,1,0,0,0,0,0
GCLC,0,0,0,0,0,0,0,0,0
NFYA,0,0,0,0,0,1,0,0,0
```

 * 要求：
   * 相关linux命令记录到脚本cmd.sh
   * 编程语言不限，代码规范简洁
