### 数据分析

#### 2021.7.7
 * 背景：日常工作内容会涉及对数据进行画图展示和算相关的统计差异值。
 * 题目说明：本题的考核利用编程生成下面的图和图中标注相关的值:
   * 样本S1： S1.clonotypes.csv
   * 样本S2： S2.clonotypes.csv
   * 其中clonotype_id为克隆型id，frequency为克隆型细胞数。

 * 要求：
   * 输出每个样品N=1（细胞数为1）的克隆型细胞数比例，以及N=2、N>=3，输出N.per.xls (15分)
   ```
   N=1 40%
   N-2 30%
   N=3 30%
   ```
   * 根据克隆型细胞数来分组，根据第三列来展示对应的差异柱状图(平均值) N.bar.png (15分)，上面数字为差异检验的Pvalue值，样式展示如下

   ![Image text](https://gitlab.com/jinbozhang08/singleronbio/-/raw/main/training/source/Picture1.jpg)


 * 参考资料
 `https://zhuanlan.zhihu.com/p/369191132`

