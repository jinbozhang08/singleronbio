### 目录
 * 背景
 * 优化
 * 测试

#### 背景

目前流程里面是遍历每个cluster然后算差异基因，如果细胞数目在10w以上的时候，分群数目30个的话，这一步计算的时间会超过1天。因此可以进行多个cluster为
1个job任务进行投递，使得物理时间降低，即进行 `cluster_num/njob` 个任务投递

原来`run_MultiSamples.V3*.R`关于差异基因计算, 其中`l`为cluster的id，1开始

```R
## 遍历每个cluster，然后做差异分析
for (l in ident){
	if(object[2]){ ###object2 FindMarkers
		cluster.markers <- FindMarkers(object=PRO,ident.1=l,min.pct=minpct_u,logfc.threshold = logfc)
		write.table(data.frame(gene_id=rownames(cluster.markers),cluster.markers),file=paste(outdir,'/',compare,'.cluster',l,'_diffgenes.xls',sep=''),sep='\t',quote=F,row.names=F)
	}
}
```

#### 优化
 * 修改原来的R程序，做差异分析之前输出RDS的levels文件从1开始的
   ```
   level_file=paste(outdir,'/',compare,'.diff_PRO.rds.levels',sep='')
   write.table(levels(x = PRO), file =level_file, sep ="\t", row.names =FALSE, col.names =FALSE, quote =FALSE)
   ```
 * 提取上面的R代码写成一个`rds2genediff.R`接受clust的参数，例如`1,2`就是算`1,2`的差异基因
 * 调用genShell4Diff.py, 生成投递脚本，用法:
   * 先cd到我们一般放的rds目录
     ```
	 cd /SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff
	 ```
   * 然后运行：
 	```
 	/SGRNJ03/pipline_test/datascience/share/01.scripts/00.Seurat/genShell4Diff.py
	```
   * 会输出下面信息：
  	```
  	工作目录为:
  	/SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff
  	输出目录为:
  	/SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff
  	工作目录找到的rds为:
  	/SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff/public_c816_NB.diff_PRO.rds
  	使用的level文件为:
  	/SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff/public_c816_NB.diff_PRO.rds.levels
  	输出结果的前缀为:public_c816_NB
  	总的聚类数目为：30, 聚类是从1开始。

  	####****** 确认上述信息对的话，执行下面的命令 ******####:

  	cd /SGRNJ03/SingleronDB/cancer/c816/results/Tumor/test4diff/shells
  	sh qsub_diffgenes.sh

  	####****** End ******####:
  	```
 
 * 如果上面信息没有错，就执行那两个命令，相关任务就会投递上了，每个任务默认的资源为`vf=50g,p=1`

#### 测试
 * 测试结果：周五13:09:41点投递上去，30个任务同时运行，**大概16:42全部结束**，每个任务大概1h ~ 3.5h, 有效降低物理时间，一个个跑大概需要60h，**约2.5day**


