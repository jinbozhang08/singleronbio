#### 2021.10.28
 * 背景：日常工作内容会涉及数据处理(包括数据库)，因为网上下载的数据会存除了字母数字之外的各种字符，例如拉丁名或者中文等:
 * 题目说明：本题考核将`readme.md`作为输入，然后输出`demo.html`, `alpha.txt`, `others.txt`。本道题30分。
 
   * 任务一：读入`readme.md`，将里面所有的中文字符按照下面的格式输出到`demo.html`, 就是替换下面的`所有中文字符`，要求40个中文字符换行(回车，不是html里面换行)(10分)

  ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="./font.css" />
   </head>
   <body>
    <h1 id="j"> 所有中文字符 </h1>
   </body>
  </html>
  ```

   * 任务二：读入`readme.md`，将里面的字母和数字按行输出到`alpha.txt`, 分成两列，第一列是行号，第二列是这一行出现的字母数字(中间不需要有空格可开)(10分)
   * 任务三：读入`readme.md`，将里面的除一和二所有的字符按行输出到`others.txt`, 分成两列，第一列是行号，第二列是这一行出现的其他字符(中间不需要有空格可开)，空格或者tab键也算其他字符(7分)
   * 输出统计信息：中文有多少个字符，字母有多少个，数字有多个，others有多少个的info.tsv文件，一共两列。(10分)
   ```
    CN  993
    ABC 88
    Num 99
    Oth 99
   ```


 * 要求：
   * 提示可以先把数字字母提取出来，然后根据中文的编码范围再把中文处理，最后剩下的为其他字符
   * 编程语言不限，需要有简单的注释说明
   * 代码规范简洁，视情况加5~10分


* 集群路径

 ```
 /opt/train/sgrbioinfo/test/data_datascience/2021.10.28/task2
 ```
 
 
