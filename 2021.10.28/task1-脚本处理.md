### 脚本考核

#### 2021.10.28
 * 背景：日常工作内容会涉及到编写shell跑任务，如果样品数目非常多或者涉及目录比较多，这个时候需要写程序来写shell，本道题30分。
 * 题目说明：本题的考核判断某个目录下的情况输出相关的shell:
   * 输入参数为当前目录，程序自动找的目录下面的相关文件夹
   * `others*`和`tcells*`是特定的任务，需要生成others.sh 和 tcells.sh
   * 每个文件夹代表一个任务，里面的stat.txt 含有关键字`running`和`done`, 如果没有stat.txt代表任务`waiting`
   * 拥有相同关键字的是同一批任务，例如others是所有others_*的根任务，`others_sub1_sub1`和`others_sub1_sub2`为`others_sub1`的子任务
   * 根据 `running`、`done`、`waiting`进行相关的脚本撰写，按目录名字最长(相等长度随机)的先输出，最后产生`others.sh`和`tcells.sh`, 如果某个任务的所有子任务都完成了，需要把这个任务当成`done`处理

```shell
## 目录结构如下
├── others
│   └── stats.txt  ## running
├── others_sub1
│   └── stats.txt  ## running
├── others_sub1_sub1
│   └── stats.txt  ## done
├── others_sub1_sub2
│   └── stats.txt  ## done 
├── others_sub2
│   └── stats.txt  ## running
├── others_sub2_sub1

## 生成类似的others.sh脚本，以tab键隔开

waiting    others_sub2_sub1
done       others_sub1_sub2
done       others_sub1_sub1
done       others_sub1
running    others_sub2
running    others
```

 * 要求：
   * 编程语言不限，需要有简单的注释说明。
   * 程序应该具有通用性，例如others的二级任务是不固定的，可能有sub1 ... subN种。例子最多是三级目录，但实际上可能出现更多级的目录
   * 代码规范简洁，可以视情况加5~10分


 * 集群路径

 ```
 /opt/train/sgrbioinfo/test/data_datascience/2021.10.28/task1
 ```
