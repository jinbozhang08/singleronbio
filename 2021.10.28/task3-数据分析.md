### 数据分析

#### 2021.7.7
 * 背景：日常工作内容会涉及对数据进行画图展示和算相关的统计差异值。本题一共40分。
 `seurat_annot.xls`文件是细胞的注释结果

   * 第一列为cellbarcode，为每个细胞的id
   * 第二列为细胞的大类注释
   * 第三列为细胞的一级细分注释
   * 第四列为细胞的二级细分注释
   * 第五列为Sample ID, 为样品ID
   * 第六列为Treatment, 为不同的治疗情况


 * 题目1：
   * 根据每一列的注释情况画出下面的饼图(细胞类型比例)和桑基图 (20分)
   * 对图片进行颜色调整或者其他美观的调整，视情况进行额外加分5~10分。
  
![Image text](https://gitlab.com/jinbozhang08/singleronbio/-/raw/main/training/source/draw.png)
  
 * 题目2：
   * 用**linux命令**，统计两种治疗情况(Post-treatment和Treatment naive)的数目，用到的命令放到cmd.sh
 
 * 题目3：
   * 以第五列的样品为单位，统计每个样品的细胞大类注释比例情况(不存在的为0)
   * 以第六列(Post-treatment和Treatment naive)把数据P和T分成两组，然后对每种细胞大类注释进行两组的T检验，以下面的格式输出
   
   ```
   P_avg    T_avg   pvalue
   ```

 * 要求：
   * 编程语言不限，需要有简单的注释说明
   * 代码规范简洁，视情况加5~10分


 * 集群路径

 ```
 /opt/train/sgrbioinfo/test/data_datascience/2021.10.28/task3
 ```
